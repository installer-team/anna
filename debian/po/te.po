# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of te.po to Telugu
# Telugu translation for debian-installer
# This file is distributed under the same license as the debian-installer package.
# Copyright (c) 2007 Rosetta Contributors and Canonical Ltd 2007
#
# Translations from iso-codes:
# వీవెన్ (Veeven) <launchpad.net>, 2007.
# Y Giridhar Appaji Nag <giridhar@appaji.net>, 2008.
# Arjuna Rao Chavala <arjunaraoc@gmail.com>,2010.
# Y Giridhar Appaji Nag <appaji@debian.org>, 2008, 2009.
# Krishna Babu K <kkrothap@redhat.com>, 2009.
# Arjuna Rao Chavala <arjunaraoc@googlemail.com>, 2011, 2012.
# Praveen Illa <mail2ipn@gmail.com>, 2018, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: te\n"
"Report-Msgid-Bugs-To: anna@packages.debian.org\n"
"POT-Creation-Date: 2021-12-19 20:01+0000\n"
"PO-Revision-Date: 2021-11-21 21:49+0000\n"
"Last-Translator: Praveen Illa <mail2ipn@gmail.com>\n"
"Language-Team: d-i <kde-i18n-doc@kde.org>\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: multiselect
#. Description
#. :sl2:
#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:1001 ../anna.templates:2001
msgid "Installer components to load:"
msgstr "లోడు చేయవలసిన స్థాపనఅంశాలు:"

#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:1001
msgid ""
"All components of the installer needed to complete the install will be "
"loaded automatically and are not listed here. Some other (optional) "
"installer components are shown below. They are probably not necessary, but "
"may be interesting to some users."
msgstr ""
"స్థాపనపూర్తి అగుటకు అవసరమయిన అంశాలు వాటి అంతట అవే లోడు చేయబడతాయి, అవి ఇక్కడ చూపటం లేదు. వేరే "
"స్థాపనఅంశాలు క్రింద చూపబడ్డాయి. ఇవి బహుశా అవసరం లేదు, కానీ కొంత మందికి ఆసక్తికరంగా ఉండవచ్చు."

#. Type: multiselect
#. Description
#. :sl2:
#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:1001 ../anna.templates:2001
msgid ""
"Note that if you select a component that requires others, those components "
"will also be loaded."
msgstr ""
"మీరు ఇతర అంశాలు కావలసిన ఒక అంశమును కనుక ఎంచుకున్నట్లయితే, ఆ అంశాలు కూడా లోడు చేయబడతాయి."

#. Type: multiselect
#. Description
#. :sl2:
#: ../anna.templates:2001
msgid ""
"To save memory, only components that are certainly needed for an install are "
"selected by default. The other installer components are not all necessary "
"for a basic install, but you may need some of them, especially certain "
"kernel modules, so look through the list carefully and select the components "
"you need."
msgstr ""
"మెమొరీ ఆదా చేయుటకు స్థాపనకు అత్యవసరమైన అంశాలు మాత్రమే మొదటగా ఎంచుకోబడ్డాయి. మౌలిక స్థాపనకు వేరే "
"స్థాపనవ్యవస్థ అంశాలు అన్నీ అవసరం ఉండకపోవచ్చు, కానీ కొన్ని అవసరం ఉండవచ్చు, ముఖ్యంగా కెర్నెల్ "
"మాధ్యమాలు. కావున జాగ్రత్తగా చూసి క్రింద జాబితా నుండి మీకు కావలసిన అంశాలు ఎన్నుకోండి."

#. Type: text
#. Description
#. (Progress bar) title displayed when loading udebs
#. TRANSLATORS : keep short
#. :sl1:
#: ../anna.templates:3001
msgid "Loading additional components"
msgstr "అదనపు అంశాలు ఎక్కించ బడుతున్నాయి"

#. Type: text
#. Description
#. (Progress bar)
#. TRANSLATORS : keep short
#. :sl1:
#: ../anna.templates:4001
msgid "Retrieving ${PACKAGE}"
msgstr "${PACKAGE}ను తెచ్చుచున్నాను"

#. Type: text
#. Description
#. (Progress bar) title displayed when configuring udebs
#. TRANSLATORS : keep short
#. :sl1:
#: ../anna.templates:5001
msgid "Configuring ${PACKAGE}"
msgstr "${PACKAGE} అమర్చుట"

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:7001
msgid "Failed to load installer component"
msgstr "స్థాపనవ్యవస్థ అంశమును లోడు చేయుట విఫలమైనది."

#. Type: error
#. Description
#. :sl2:
#: ../anna.templates:7001
msgid "Loading ${PACKAGE} failed for unknown reasons. Aborting."
msgstr "తెలియని కారణాల వల్ల ${PACKAGE}ను లోడు చేయుట విఫలమైనది. విరమించుచున్నాను."

#. Type: boolean
#. Description
#. :sl2:
#: ../anna.templates:8001
msgid "Continue the install without loading kernel modules?"
msgstr "కెర్నెల్ మాధ్యమాలు లోడు చెయ్యకుండా స్థాపనను కొనసాగించవలెనా?"

#. Type: boolean
#. Description
#. :sl2:
#: ../anna.templates:8001
msgid ""
"No kernel modules were found. This probably is due to a mismatch between the "
"kernel used by this version of the installer and the kernel version "
"available in the archive."
msgstr ""
"కెర్నెల్  మాడ్యులులు కనబడలేదు. ఈస్థాపన వ్యవస్థ విడుదలలో వున్న కెర్నెల్ సంఖ్యకు, సంగ్రహం లో వున్న "
"కెర్నెల్ విడుదల  సంఖ్యకు తేడా వుండటం వలన  ఇలా జరిగ వుండవచ్చు. "

#. Type: boolean
#. Description
#. :sl2:
#: ../anna.templates:8001
msgid ""
"You should make sure that your installation image is up-to-date, or - if "
"that's the case - try a different mirror, preferably deb.debian.org."
msgstr ""

#. Type: boolean
#. Description
#. :sl2:
#: ../anna.templates:8001
#, fuzzy
msgid ""
"The installation will probably fail to work if you continue without kernel "
"modules."
msgstr ""
"మిర్రర్ నుండి స్థాపన చేస్తున్నట్లయితే, ఈ సమస్యకి  పరిష్కారంగా, వేరొక డెబియన్ విడుదల సంఖ్యని  "
"స్థాపించవచ్చు. కెర్నెల్  మాడ్యులులు లేకుండా మీరు కొనసాగించినట్లయితే ఈ స్థాపన  బహుశా పనిచేయకపోవచ్చు, "
